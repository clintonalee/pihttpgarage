﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace PiHttpGarage.Controllers
{
    [Route("status")]
    public class StatusController : Controller
    {
        private readonly GarageDoorService garageDoorService;

        public StatusController(GarageDoorService garageDoorService)
            => this.garageDoorService = garageDoorService;

        [Route("")]
        public async Task<IActionResult> Status()
        {
            var (currentState, targetState) = await garageDoorService.GetState();
            return Ok(new
            {
                //for legacy, homebridge-http-garage, adapter
                currentState = (int) targetState,

                //homebridge-web-garage adapter properties
                currentDoorState = (int) currentState,
                targetDoorState = (int) targetState
            });
        }
    }
}
