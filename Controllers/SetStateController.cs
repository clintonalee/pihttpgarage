﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace PiHttpGarage.Controllers
{
    [Route("")]
    public class SetStateController : Controller
    {
        private readonly GarageDoorService garageDoorService;

        public SetStateController(GarageDoorService garageDoorService)
            => this.garageDoorService = garageDoorService;
        
        [Route("setState")]
        //this is what homebridge-web-garage docs say
        [Route("targetDoorState")]
        //... and this is the page in their code
        [Route("setTargetDoorState")]
        public async Task<IActionResult> SetState(int @value)
        {
            if(value == 0)
            {
                await garageDoorService.Open();
                return Ok();
            }
            else if (value == 1)
            {
                await garageDoorService.Close();
                return Ok();
            }
            else
            {
                return BadRequest(new
                {
                    invalidValue = value
                });
            }
        }
    }
}
