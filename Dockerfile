FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster-arm32v7 AS builder

WORKDIR /src

COPY PiHttpGarage.csproj /src/PiHttpGarage.csproj

RUN dotnet restore PiHttpGarage.csproj

COPY . /src/

RUN dotnet publish -c Release -o /build --no-restore PiHttpGarage.csproj

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim-arm32v7

LABEL maintainer "Clinton Lee <clintonalee@gmail.com>"

WORKDIR /app

ENTRYPOINT [ "dotnet", "PiHttpGarage.dll" ]

COPY --from=builder /build /app
