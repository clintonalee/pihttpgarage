﻿using System;
using System.Device.Gpio;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace PiHttpGarage
{
    public enum TargetDoorState
    {
        Open = 0,
        Closed = 1
    }
    public enum GarageDoorState
    {
        Open,
        Closed,
        Opening,
        Closing,
        Stopped
    }

    public class GarageDoorService : IDisposable
    {
        private static readonly TimeSpan OPEN_CLOSE_DURATION = TimeSpan.FromSeconds(25);

        private readonly object actionLock = new object();
        private Task action;

        private GarageDoorState currentDoorState = GarageDoorState.Closed;
        private TargetDoorState targetDoorState = TargetDoorState.Closed;

        private readonly int pinNumber;
        private readonly string homebridgeHttpWebhookUrl;
        private readonly GpioController gpioController;
        private readonly string stateListenerUrl;

        private async Task SafeCallStateListenerUrl(string pathAndQuery)
        {
            if(string.IsNullOrEmpty(stateListenerUrl))
            {
                return;
            }
            using var wc = new WebClient();
            var url = $"{stateListenerUrl}{pathAndQuery}";
            try
            {
                var resp = await wc.DownloadStringTaskAsync(url);
                if(!string.IsNullOrEmpty(resp))
                {
                    await Console.Out.WriteLineAsync($"[INFO] called {url} and response was: {resp}");
                }
            }
            catch (Exception ex)
            {
                await Console.Error.WriteLineAsync($"[ERROR] calling {url}: {ex}");
            }
        }

        private async Task SafeCallHomebridgeHttpWebhook(string extraQueryString)
        {
            if (string.IsNullOrEmpty(homebridgeHttpWebhookUrl))
            {
                return;
            }
            using var wc = new WebClient();
            var url = $"{homebridgeHttpWebhookUrl}{extraQueryString}";
            try
            {
                var resp = await wc.DownloadStringTaskAsync(url);
                if (!string.IsNullOrEmpty(resp))
                {
                    await Console.Out.WriteLineAsync($"[INFO] called {url} and response was: {resp}");
                }
            }
            catch (Exception ex)
            {
                await Console.Error.WriteLineAsync($"[ERROR] calling {url}: {ex}");
            }
        }

        private async Task SetTargetDoorState(TargetDoorState state)
        {
            targetDoorState = state;
            await Task.WhenAll(
                SafeCallStateListenerUrl($"/targetDoorState?value={(int)state}"),
                SafeCallHomebridgeHttpWebhook($"&targetdoorstate={(int)state}"));
        }

        private async Task SetCurrentDoorState(GarageDoorState state)
        {
            currentDoorState = state;
            await Task.WhenAll(
                SafeCallStateListenerUrl($"/currentDoorState?value={(int)state}"),
                SafeCallHomebridgeHttpWebhook($"&currentdoorstate={(int)state}"));
        }

        private Task PushButton()
            => Task.Run(() =>
            {
                lock (gpioController)
                {
                    gpioController.OpenPin(pinNumber, PinMode.Output);
                    try
                    {
                        gpioController.Write(pinNumber, PinValue.Low);
                        Thread.Sleep(1000);
                    }
                    finally
                    {
                        gpioController.ClosePin(pinNumber);
                    }
                }
            });

        private async Task MoveDoorToState(TargetDoorState targetState, Task previousTask)
        {
            if(previousTask != null)
            {
                try
                {
                    await previousTask;
                }
                catch
                {
                    //incase our previous task had an error. We can move on.
                }
            }

            //are we heading in that direction? If so, then we can skip
            if(
                (targetState == TargetDoorState.Open && targetDoorState == TargetDoorState.Open)
                || (targetState == TargetDoorState.Closed && targetDoorState == TargetDoorState.Closed))
            {
                return;
            }

            await SetTargetDoorState(targetState);
            await SetCurrentDoorState(targetDoorState switch
            {
                TargetDoorState.Open => GarageDoorState.Opening,
                _ => GarageDoorState.Closing,
            });
            await PushButton();
            await Task.Delay(OPEN_CLOSE_DURATION);
            await SetCurrentDoorState(targetDoorState switch
            {
                TargetDoorState.Open => GarageDoorState.Open,
                _ => GarageDoorState.Closed
            });
        }

        public GarageDoorService(int pinNumber = 21, string stateListenerUrl = null, string homebridgeHttpWebhookUrl = null)
        {
            gpioController = new GpioController();
            this.pinNumber = pinNumber;
            this.homebridgeHttpWebhookUrl = homebridgeHttpWebhookUrl;
            this.stateListenerUrl = stateListenerUrl?.Trim()?.TrimEnd('/');
        }

        private Task SetState(TargetDoorState newState)
        {
            lock(actionLock)
            {
                action = MoveDoorToState(newState, action);
            }
            return Task.CompletedTask;
        }

        public async Task Open() => await SetState(TargetDoorState.Open);

        public async Task Close() => await SetState(TargetDoorState.Closed);

        public Task<(GarageDoorState current, TargetDoorState target)> GetState()
            => Task.FromResult((current: currentDoorState, target: targetDoorState));

        public void Dispose()
        {
            gpioController?.Dispose();
        }
    }
}
